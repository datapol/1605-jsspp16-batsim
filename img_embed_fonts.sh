#!/bin/sh

# Usage ./$0 source dest

 gs -dCompatibilityLevel=1.4 \
    -dPDFSETTINGS=/screen \
    -dCompressFonts=true \
    -dSubsetFonts=true \
    -dNOPAUSE \
    -dBATCH \
    -sDEVICE=pdfwrite \
    -sOutputFile=$2 \
    -c ".setpdfwrite <</NeverEmbed [ ]>> setdistillerparams" \
    -f $1
