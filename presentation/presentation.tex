% rubber: setlist arguments --shell-escape

\documentclass[]{beamer}

\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=,urlcolor=links}

\mode<presentation>
{
  \usetheme{Frankfurt}
  \usecolortheme{default}
  \beamertemplatenavigationsymbolsempty
  \setbeamertemplate{caption}{\insertcaption}
}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
%\usepackage{pgfplots}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{xargs}
\usepackage{minted}
\usepackage{MnSymbol}
\usepackage[yyyymmdd]{datetime}
\usepackage{pgfpages}
\usepackage{wrapfig}
\usepackage{adjustbox}

\renewcommand{\dateseparator}{--}
\setbeamertemplate{footline}[frame number]

\title{Batsim: a Realistic Language-Independent Resources and Jobs Management Systems Simulator}

\author{Pierre-François Dutot\inst{1}, \underline{Michael Mercier\inst{1,2}}, Millian
Poquet\inst{1}, Olivier
Richard\inst{1}}
\institute{DATAMOVE Team, LIG\\
  \includegraphics[width=0.25\textwidth]{./img/logo_UGA.png}
  \hspace{5mm}
  \includegraphics[width=0.25\textwidth]{./img/logo_INRIA.png}
  \and
  \includegraphics[width=0.25\textwidth]{./img/logo_Bull.png}
}
\date[JSSPP'2016]{\small 20th JSSPP Workshop, May 27 2016, Chicago}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\section{Motivation}
\subsection{General Context}
\begin{frame}[fragile]{HPC cluster management}
  \begin{wrapfigure}{r}{0.20\textwidth} %this figure will be at the right
    \centering
    \includegraphics[width=0.22\textwidth]{img/hpc_cluster.png}
  \end{wrapfigure}
  Resources and Jobs Management Systems (RJMS)
  \begin{itemize}
    \item AKA batch scheduler
    \item Orchestrates resources on HPC clusters
    \begin{itemize}
      \item Implements scheduling policies
      \item Manages parallel jobs
      \item Enforces energy policy
    \end{itemize}
    \item Examples: SLURM, OAR, TORQUE, PBS\dots
  \end{itemize}
  \begin{block}{RJMS Facts}
    \begin{itemize}
      \item Large scale: from 100 to 100 000 nodes
      \item Critical production system: downtime is \$
      \item Need to be energy efficient: HPC energy consumption in MW
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Improve the RJMS's schedulers}
  Many parameters to consider:
  \begin{itemize}
    \item scheduling policies
    \item platform
    \item workloads
    \item metrics
  \end{itemize}
  \textbf{We need to experiment on the RJMS but\dots}
  \begin{alertblock}{Production systems are not available for testing RJMS}
    \begin{itemize}
      \item They are already full of users jobs!
      \item Energy/time cost of experiments is not affordable
    \end{itemize}
  \end{alertblock}
\end{frame}

\subsection{RJMS Simulation}
\begin{frame}[fragile]{Simulation at rescue}
  \begin{itemize}
    \item Easy to select workloads, platforms
    \item Fast comparison of algorithms
    \begin{itemize}
      \item Real: 33 compute nodes for 3 hours
      \item Simu: 1 laptop for 3 seconds
    \end{itemize}
    \item Closest approches
    \begin{itemize}
      \item ALEA (Klus\'{a}\v{c}ek and Rudov\'{a})
      \item INSEE based simulator (Pascual et al.)
    \end{itemize}
  \end{itemize}

  \begin{block}{Common difficulties}
    \begin{itemize}
      \item Precision vs. Performance
      \item Validation: comparison with reality
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{\textbf{Batsim}: \textbf{Bat}ch scheduler \textbf{sim}ulator}
  Focus on realism, facilitate comparison
  \begin{exampleblock}{Batsim's approach}
    \begin{itemize}
      \item Realistic network model\\
        $\rightarrow$ \textbf{SimGrid network models, topologies}
      \item Multiple job models\\
        $\rightarrow$ \textbf{From simple to realistic}
      \item Openness\\
        $\rightarrow$ \textbf{Open source and documented}
      \item Modularity\\
        $\rightarrow$ \textbf{Language-agnostic text-based protocol}
      \item Easy to evaluate and compare\\
        $\rightarrow$ \textbf{Any event-based algorithm can be plugged}\\
        $\rightarrow$ \textbf{Including real RJMS ones!}
    \end{itemize}
  \end{exampleblock}
\end{frame}

\section{How it works}
\begin{frame}[fragile]{Outline}
  \tableofcontents[sections={1-4}, hidesubsections]
\end{frame}

\subsection{General Architecture}

\begin{frame}{Batsim Overview}
  \begin{itemize}
    \item Core program ($\simeq$ 3k lines of C++ code)
    \item Several schedulers already adapted
    \begin{itemize}
      \item In Python, Perl, C++
      \item More than 15 scheduling and energy policies
      \item From production and research
    \end{itemize}
    \item Simulates a RJMS and the compute nodes
    \item Scheduler in another process
    \item Based on the SimGrid framework
    \begin{itemize}
      \item Reliable: Has been used for 15+ years, strong community
      \item Scalable
      \item Topology aware network models
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Real vs. Simulated}
  \begin{figure}[h]
      \centering
      \includegraphics[width=\textwidth]{img/batsim_overview.pdf}
    \end{figure}
\end{frame}

\begin{frame}[fragile]{Batsim inputs}
  \begin{wrapfigure}{r}{0.35\textwidth} %this figure will be at the right
    \centering
    \includegraphics[width=0.3\textwidth]{img/Fat_tree_network.png}\\
    \vspace{5mm}
    \includegraphics[width=0.3\textwidth]{img/workloads.pdf}
  \end{wrapfigure}
  \textbf{What is a Batsim platform?}
  \begin{itemize}
    \item Batsim platform $\simeq$ SimGrid platform
  \end{itemize}
  \vspace{2cm}
  \textbf{What is a Batsim workload?}
  \begin{itemize}
    \item List of jobs
    \begin{itemize}
      \item Submit time
      \item Walltime (user-given maximum runtime)
      \item Required resources
    \end{itemize}
    \item Each job is associated to a profile
  \end{itemize}
\end{frame}

\begin{frame}{Job Profile types}
    \begin{description}
      \item[Delay]
      \begin{itemize}
        \item Fixed amount of time\vspace{5mm}
      \end{itemize}
      \item[MSG]
      \begin{itemize}
        \item A computation vector (1D matrix)
        \item A communication 2D matrix\vspace{5mm}
      \end{itemize}
      \item[Sequence]
      \begin{itemize}
        \item A sequence of profiles
        \item Repeated $n$ times
        \item à la BSP\footnote{Bulk Synchronous Parallel model}\vspace{5mm}
      \end{itemize}
    \end{description}
\end{frame}

\subsection{Communication Protocol}
\begin{frame}{Communication Protocol}
  \hspace{-1cm}
  \begin{minipage}[r]{.50\linewidth}
    \begin{figure}
      \centering
      \includegraphics[width=1.2\textwidth]{img/protocol_sequence.pdf}
    \end{figure}
  \end{minipage}
  \hspace{1.6cm}
  \begin{minipage}[l]{.40\linewidth}
    \begin{block}{Protocol characteristics}
      \begin{itemize}
        \item Textual
        \item Synchronous
        \item Request-Reply pattern
        \item Unix Domain Socket
        \item More info in the doc
      \end{itemize}
    \end{block}
  \end{minipage}
\end{frame}

\section{Evaluation}
\begin{frame}[fragile]{Outline}
  \tableofcontents[sections={1-4}, currentsection]
\end{frame}

\subsection{Evaluation process description}
\begin{frame}[fragile]{Evaluation process}
  Same scheduler run \textbf{in vivo} and \textbf{in simulo}\\
  $\rightarrow$ Compare the metrics\\
  \begin{block}{A chicken and egg situation}
    \begin{minipage}{3.5cm}
      \includegraphics[width=0.9\textwidth]{img/Chicken_and_egg.pdf}
    \end{minipage}%
    \begin{minipage}{7cm}
    We need simulation to save \$\\
    But we need \$ to validate the simulator
    \end{minipage}%
  \end{block}
  Our evaluation process is \textbf{reproducible}
\end{frame}

\subsection{Our evaluation}
\renewcommand{\arraystretch}{1.2}
\begin{frame}{Experiment design - in vivo vs. in simulo}
  \centering
  \hspace*{-7mm}
  \adjustbox{max height=\textheight, max width=1.1\linewidth}{
  \begin{tabular}{lll}
  \cline{2-3}
                       & \multicolumn{1}{l}{\textbf{In Vivo}}                                                                                                                    & \textbf{In Simulo}                                                                                                                                  \\ \hline \hline
  \textbf{Platform}    & \multicolumn{1}{l|}{Grid'5000 testbed cluster}                                                                                                           & SimGrid XML platform                                                                                                                                \\ \cline{2-3}
  & \multicolumn{2}{c}{\alert{33 nodes under 1 switch}}                                                                                                                                                                                                                                            \\ \hline
  \textbf{Jobs}        & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}NAS Parallel Benchmarks\\ Type: IS, FT and LU\end{tabular}}                                               & \begin{tabular}[c]{@{}l@{}}Job profile generated from\\ real execution instrumentation\end{tabular}                                                 \\ \cline{2-3}
  \textbf{}            & \multicolumn{2}{c}{\begin{tabular}[c]{@{}c@{}}Size: from 1 to 32 nodes\\ Total: \alert{47 different jobs}\end{tabular}}                                                                                                                                                                                           \\ \hline
  \textbf{Workloads}   & \multicolumn{2}{c}{\begin{tabular}[c]{@{}c@{}}Contains \alert{800 jobs} randomly picked\\ Inter arrival times: Weibull dist. ($shape = 2$, $scale = 15$)\\ Job size: $2^{\lfloor u \rfloor}$, $u\sim$ Lognormal dist.($\mu=0.25$, $\sigma=0.5$)\\ Total: \alert{9 workloads $\approx$4h each}\end{tabular}}\\ \cline{2-3} & \multicolumn{1}{l|}{2 runs/workload}                                                                                                           & 1 deterministic simulation                                                                                                                                \\ \hline

  \textbf{Scheduler}   & \multicolumn{2}{c}{\href{https://github.com/oar-team/oar3/blob/master/oar/kao/kamelot.py}{Kamelot}: scheduler from OAR (conservative backfilling)}                                                                                                                                                              \\ \cline{2-3}
  \textbf{}            & \multicolumn{1}{l|}{Directly executed by OAR}                                                                                                            & \href{https://github.com/oar-team/oar3/blob/master/oar/kao/bataar.py}{BatAar}: BatSim Adaptor for OAR                                               \\ \hline
  \textbf{Resources} & \multicolumn{1}{l|}{10 000+ hour$\times$cores}                                                                                                         & Only 30s on a laptop
  \end{tabular}
  }
\end{frame}

\begin{frame}[fragile]{Evaluation instantiation}
  \begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{img/batsim_overview_expe.pdf}
  \end{figure}
\end{frame}

\subsection{Results - Gantt charts}
\begin{frame}[fragile]{Gantt charts is not enough}
  \hspace*{-10mm}
  \includegraphics[width=1.18\textwidth]{results/gantt_comparison.pdf}
\end{frame}

\subsection{Results - Metrics}
\begin{frame}[fragile]{Use metrics}
  We need aggregated metrics:
  \begin{block}{Makespan AKA Cmax}
    \centering
    $max_i(FinishT_i)$
  \end{block}
  \begin{block}{Mean waiting time}
    \centering
    $\frac{1}{nbJobs} \sum_i{StartT_i - SubmitT_i}$
  \end{block}
  \begin{block}{Mean bounded slowdown}
    \centering
    $\frac{1}{nbJobs} \sum_i{max(\frac{FinishT_i - SubmitT_i}{max_i(K, FinishT_i - StartT_i)}},1)$
  \end{block}
  Real life is not deterministic:
  \begin{itemize}
    \item Every real execution different
    \item Simulation is deterministic
    \item $\rightarrow$ It must be representative
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Metrics comparison - One workload}
  \includegraphics[width=\textwidth]{results/graphs/aggregated_comparison_mean_bounded_stretch_one_workload_zoomed.pdf}
\end{frame}

\begin{frame}[fragile]{Metrics comparison - All workloads}
  \includegraphics[width=\textwidth]{results/graphs/aggregated_comparison_mean_bounded_stretch.pdf}
\end{frame}

\begin{frame}[fragile]{Metrics comparison - Zoom out}
  \includegraphics[width=\textwidth]{results/graphs/aggregated_comparison_mean_bounded_stretch_zero_zero.pdf}
\end{frame}

%\begin{frame}[fragile]{Metrics comparison - Difference overview}
%    \includegraphics[width=\textwidth]{results/graphs/mean_waiting_time_reality_difference_histogram.pdf}\\
%\end{frame}
%
%\begin{frame}[fragile]{Metrics comparison - Difference overview}
%    \includegraphics[width=\textwidth]{results/graphs/makespan_reality_difference_histogram.pdf}
%\end{frame}
%
%\begin{frame}[fragile]{Metrics comparison - Difference overview}
%    \includegraphics[width=\textwidth]{results/graphs/mean_bounded_stretch_reality_difference_histogram.pdf}\\
%\end{frame}

\begin{frame}[fragile]{Metrics comparison - Difference overview}
  \vspace*{-1mm}
  \begin{columns}[t]
    \column{.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{results/graphs/makespan_reality_difference_histogram.pdf}\\
    \includegraphics[width=\textwidth]{results/graphs/mean_waiting_time_reality_difference_histogram.pdf}
    \column{.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{results/graphs/mean_bounded_stretch_reality_difference_histogram.pdf}\\
    \begin{block}{Differences}
      \begin{itemize}
        \item Waiting times are underestimated
        \item \textbf{Beware of overfitting OAR's behaviour}
      \end{itemize}
    \end{block}
  \end{columns}
\end{frame}

\section{Conclusion}
\begin{frame}[fragile]{Outline}
  \tableofcontents[sections={1-4}, currentsection]
\end{frame}

\subsection{Results summary}
\begin{frame}{Results discussion}
  \begin{exampleblock}{Done}
    \begin{itemize}
      \item Same scheduler run in vivo and in simulo
      \begin{itemize}
      \item Delay and MSG model
      \item Similar behavior on classic metrics
      \item Small workloads
      \item Small homogeneous platform
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
  \begin{block}{Next steps}
    \begin{itemize}
      \item Test other schedulers in vivo and in simulo (e.g. Flux scheduler)
      \item Characterize real experiment noise (\$\$) (in progress)
      \item Heterogeneous platforms (\$\$)
      \item Big platforms (\$\$\$\$\$)
      \item Validate the energy models
      \item Include IO simulation ($\rightarrow$ big data workload simulation)
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Thanks!}
  \begin{columns}[t]
    \column{.55\textwidth}
      \textbf{Batsim:}\\
      \href{https://github.com/oar-team/batsim}{https://github.com/oar-team/batsim}\\
      \vspace{5mm}
      \begin{block}{Contacts}
      michael.mercier@inria.fr\\
      millian.poquet@inria.fr
      \end{block}
    \column{.4\textwidth}
    \vspace{5mm}
    \centering
    \includegraphics[width=\textwidth]{img/sponsors_logo.png}
  \end{columns}
  \vspace{5mm}
  References:
  \tiny
  \begin{itemize}
    \item Dalibor Klusáček, Hana Rudová. \textbf{Alea 2 - Job Scheduling
      Simulator.} In proceedings of the 3rd International ICST Conference on Simulation Tools and Techniques (SIMUTools 2010), ICST, 2010.
    \item Jose A. Pascual, Jose Miguel-Alonso, Jose A. Lozano. \textbf{Locality-aware policies to improve job scheduling on 3D tori.} The Journal of Supercomputing, 2015, vol. 71, no 3, p. 966-994.
    \item D.G. Feitelson. \textbf{Workload Modeling for Computer Systems
      Performance Eval- uation.} Cambridge University Press, Cambridge,
      2015.\\
  \end{itemize}
\end{frame}

\appendix
\newcounter{finalframe}
\setcounter{finalframe}{\value{framenumber}}
\section*{Backup slides}
\subsection{Workload example}
\begin{frame}[fragile]{Workload File Example}
  \inputminted[fontsize=\scriptsize]{JSON}{code/workload_example.json}
\end{frame}

\subsection{Inner Mechanics}
\begin{frame}{Inner Mechanics overview}
  \begin{block}{What is inside Batsim?}
    \begin{itemize}
      \item Batsim is NOT a simple event loop
      \item Batsim = many \textit{processes} which interact with each other
    \end{itemize}
  \end{block}
  \onslide{
    \begin{figure}[h]
      \centering
      \includegraphics[width=.8\textwidth]{img/batsim_inner_processes.pdf}
    \end{figure}
  }
\end{frame}

\begin{frame}[fragile]{Inner Mechanics details}
  Why does Batsim use such a design?
    \begin{itemize}
      \item Permit fine-grained RJMS simulation
      \item More complex, but more modular
      \item Individual parts are well separated and remain simple
      \item Specific parts can be detailed, without modifying the other parts (e.g. scheduling time, job launchs...)
      \item More complex designs can be done (e.g. distributed RJMS...)
    \end{itemize}
\end{frame}

\setcounter{framenumber}{\value{finalframe}}
\end{document}
