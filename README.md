1605-jsspp16-batsim
----------------------------

Repository to generate the ` Batsim: A Realistic Language-Independent Resources and Jobs Management Systems Simulator ` article
published at JSSPP'2016. https://hal.inria.fr/hal-01333471v1

How to build
------------

1. Install dependencies (latex suite, rubber, make)
2. Build pdf: `make`
