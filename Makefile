article:
	rubber -d -Wboxes -Wrefs -Wmisc -Wall batsim.tex

bw: article
	gs \
   -sDEVICE=pdfwrite \
   -sProcessColorModel=DeviceGray \
   -sColorConversionStrategy=Gray \
   -dOverrideICC \
   -o batsim_bw.pdf \
   -f batsim.pdf

clean:
	rubber -d --clean batsim.tex

mrproper: clean
	rm -f batsim.pdf
	rm -f batsim_bw.pdf
